window.onload = setTimeout(function () {
    document.getElementById("loader").style.opacity = "0";
}, 2000);
window.onload = setTimeout(function () {
    document.getElementById("main-page").style.display = "block";
    document.getElementById("loader").style.display = "none";
}, 3000);

window.onload = setTimeout(function () {
    for (var i = 0; i < fade_on_scroll.length; i++) {
        if (fade_on_scroll[i].getBoundingClientRect().top < window.innerHeight) {
            fade_on_scroll[i].style.transition = "opacity 1s ease-in-out";
            fade_on_scroll[i].style.opacity = "1";
        }
    }
}, 3050);

function openNav() {
    document.getElementById("sidebar").style.width = "250px";
}

function closeNav() {
    document.getElementById("sidebar").style.width = "0";
}

var fade_on_scroll = document.getElementsByClassName('fade-on-scroll');


window.addEventListener('scroll', function () {
    for (var i = 0; i < fade_on_scroll.length; i++) {
        if (fade_on_scroll[i].getBoundingClientRect().top < window.innerHeight) {
            fade_on_scroll[i].style.transition = "opacity 1s ease";
            fade_on_scroll[i].style.opacity = "1";
        }
    }
});