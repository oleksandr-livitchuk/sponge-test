﻿'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    prefixer = require('gulp-autoprefixer'),
    cssmin = require('gulp-clean-css'),
    uglify = require('gulp-uglify'),
    jshint = require("gulp-jshint"),
    rimraf = require('rimraf'),
    sourcemaps = require('gulp-sourcemaps'),
    rename = require("gulp-rename"),
    plumber = require("gulp-plumber"),
    watch = require('gulp-watch'),
    imagemin = require('gulp-imagemin'),
    browsersync = require('browser-sync').create();


var path = {
    build: {
        html: 'build/',
        js: 'build/js/',
        css: 'build/css/',
        img: 'build/css/images/',
        fonts: 'build/fonts/'
    },
    src: {
        html: 'src/*.html',
        js: 'src/js/[^_]*.js',
        jshint: 'src/js/*.js',
        css: 'src/css/styles.scss',
        img: 'src/css/images/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    watch: {
        html: 'src/*.html',
        js: 'src/js/**/*.js',
        css: 'src/css/**/*.*',
        img: 'src/css/images/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    clean: './build',
    outputDir: './build'
};

gulp.task('browsersync', function(){
    browsersync.init({
        server: [path.outputDir]
    });
});

gulp.task('image:build', function () {
    gulp.src(path.src.img)
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.jpegtran({progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(gulp.dest(path.build.img))
        .pipe(browsersync.stream())
});

gulp.task('html:build', function () {
    gulp.src(path.src.html)
        .pipe(plumber())
        .pipe(gulp.dest(path.build.html))
        .pipe(browsersync.stream())
});


gulp.task('jshint:build', function() {
    return gulp.src(path.src.jshint)
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'));
});


gulp.task('js:build', function () {
    gulp.src(path.src.js)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(path.build.js))
        .pipe(browsersync.stream())
});



gulp.task('css:build', function () {
    gulp.src(path.src.css)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(prefixer({
            browsers: ['last 3 version', "> 1%", "ie 10"]
        }))
        .pipe(cssmin())
        .pipe(sourcemaps.write())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(path.build.css))
        .pipe(browsersync.stream())
});



gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(plumber())
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('build', [
    'html:build',
    'jshint:build',
    'js:build',
    'css:build',
    'fonts:build',
    'image:build'
]);


gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});


gulp.task('watch', function(){

    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });

    watch([path.watch.css], function(event, cb) {
        gulp.start('css:build');
    });

    watch([path.watch.js], ['jshint']);

    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });

    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
});


gulp.task('default', ['build', 'watch', 'browsersync']);
